import sys, pyltr
from pyltr.models import AdaRank, LambdaMART
from math import sqrt

import numpy as np
import datetime
from sklearn.preprocessing import StandardScaler

from pyltr.email_sender import send_message
from pyltr.util import data as data_load
from pyltr.util.metrics import mean_ndcg

# Your email details
fromaddr = "jcjuli@gmail.com"

def write_to_file(file, text):
    f = open(file, 'a')
    f.write(text)
    f.close()


def main(argv):
    import argparse

    parser = argparse.ArgumentParser(description='Ranking configuration setting for Pointwise Approach')
    parser.add_argument("-d","--dataset", default = 'all', choices = ['letor', 'yahoo', 'mslr10k','all'], help = 'dataset list options to run the models (default: %(default)s)',required=True)
    parser.add_argument("-f","--fold", help = 'specified the fold to process', required=True)
    args = parser.parse_args()

    # Email message
    toaddr = "juliancacrs@gmail.com"
    print(args.dataset)
    out_file_name = '%s_fold%s_listwise_results.csv'%(args.dataset, args.fold)
    f = open(out_file_name, 'a')
    f.write('Fold,algorithm,rmse,ndcg10,ndgc5,ndcg1,ndcg_score,time\n')
    f.close()
    # defining quantity of folds
    metric_adarank = pyltr.util.adarank_metrics.NDCGScorer(k=10)
    metric_lm = pyltr.metrics.NDCG(k=10)
    K = 5
    k = int(args.fold)
    if  args.dataset == 'yahoo':
        K = 2
        n_features = 26
    elif  args.dataset == 'mslr10k':
        n_features = 11
    else:
        n_features = 6
    adarank = AdaRank(max_iter=100, estop=10, verbose=False, scorer=metric_adarank)
    lambdamart= LambdaMART(metric=metric_lm,n_estimators=100, learning_rate=0.02, max_features=0.2, query_subsample=0.4,
                           max_leaf_nodes=10, min_samples_leaf=64, verbose=0,)
    models = [
        ('adarank', adarank),
        ('lambdamart', lambdamart)
    ]

    print('Starting process for the %s dataset on  Fold %d'%(args.dataset, k))
    if args.dataset == 'letor':
        dataset = data_load.get_dataset_letor(k)
    elif args.dataset == 'yahoo':
        dataset = data_load.get_dataset_yahoo(k)
    elif args.dataset == 'mslr10k':
        dataset = data_load.get_dataset_mslr10k(k)
    else:
        print('Option no valid you need to pick only 1 data set at time')
        return False
    TX, Ty, Tqids = dataset['TX'],dataset['Ty'],dataset['Tqids']
    EX, Ey, Eqids = dataset['EX'], dataset['Ey'], dataset['Eqids']
    VX, Vy, Vqids = dataset['VX'], dataset['Vy'], dataset['Vqids']

    # scale dataset to convert to normalize it.
    scaler = StandardScaler()
    Xtrain = scaler.fit_transform(TX)
    Xtest = scaler.transform(EX)

    body = ""
    subject = "Message about the Fold%d" % (k)
    for j, (name, model) in enumerate(models):
        print(subject,' for model',name )
        start = datetime.datetime.now()
        if name is 'adarank':
            continue
            #model.fit(Xtrain, Ty, Tqids, VX, Vy, Vqids)
            #pred = model.predict(Xtest, Eqids)
        else:
            model.fit(TX, Ty, Tqids)
            pred = model.predict(Xtest)


        # Getting different kind of scores.
        mse = np.mean((pred - Ey) ** 2)
        rmse =sqrt(mse)
        ndcg_10 = mean_ndcg(Ey, pred, Eqids, rank=10)
        ndcg_5 = mean_ndcg(Ey, pred, Eqids, rank=5)
        ndcg_1 = mean_ndcg(Ey, pred, Eqids, rank=1)
        ndcg_all = mean_ndcg(Ey, pred, Eqids, rank=None)
        stop = datetime.datetime.now()
        time_ = stop - start

        body_corpus = "\n-----------------%s----------------------------\n\n"%(name)
        body_corpus = body_corpus + "RMSE value = %.3f\n"%(rmse)
        body_corpus = body_corpus + "NDCG 10 value = %.3f\n"%(ndcg_10)
        body_corpus = body_corpus + "NDCG 5 value = %.3f\n"%(ndcg_5)
        body_corpus = body_corpus + "NDCG 1 value = %.3f\n"%(ndcg_1)
        body_corpus = body_corpus + "NDCG ALL value = %.3f\n"%(ndcg_all)
        body_corpus = body_corpus + "Running time  = {0} \n".format(time_)
        body = body + body_corpus
        # writing the results on a csv file.
        row_file = str(k)+','+name+','+str(rmse)+','+str(ndcg_10)+','+str(ndcg_5)+','+str(ndcg_1)+','+str(ndcg_all)+','+str(time_)+'\n'
        #testing
        print(body_corpus)
        write_to_file(out_file_name, row_file)

        if hasattr(model, '_current_predicted'):
            model._current_predicted = None
    send_message(fromaddr, toaddr, subject, body)

if __name__ == "__main__":
    main(sys.argv[1:])

