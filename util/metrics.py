import numpy as np

def dcg(relevances, rank=10):
    """Discounted cumulative gain at rank (DCG)"""
    relevances = np.asarray(relevances)[:rank]
    n_relevances = len(relevances)
    if n_relevances == 0:
        return 0.

    discounts = np.log2(np.arange(n_relevances) + 2)
    dcg = np.sum(relevances / discounts)
    return dcg


def ndcg(relevances, rank=10):
    """Normalized discounted cumulative gain (NDGC)"""
    relevances_ = relevances.copy()
    del relevances
    best_dcg = dcg(sorted(relevances_, reverse=True), rank)
    if best_dcg == 0:
        return 0.

    return dcg(relevances_, rank) / best_dcg

def mean_ndcg(y_true, y_pred, query_ids, rank=10):
    y_true = np.asarray(y_true)
    y_pred = np.asarray(y_pred)
    query_ids = np.asarray(query_ids)
    # assume query_ids are sorted
    ndcg_scores = []
    ranked_relevances = []
    previous_qid = query_ids[0]
    previous_loc = 0
    for loc, qid in enumerate(query_ids):
        if previous_qid != qid:
            chunk = slice(previous_loc, loc)
            ranked_relevances = y_true[chunk][np.argsort(y_pred[chunk])[::-1]]
            ndgc_ = ndcg(ranked_relevances, rank=rank)
            ndcg_scores.append(ndgc_)
            previous_loc = loc
        previous_qid = qid

    chunk = slice(previous_loc, loc + 1)
    ranked_relevances = y_true[chunk][np.argsort(y_pred[chunk])[::-1]]
    ndcg_scores.append(ndcg(ranked_relevances, rank=rank))
    mean_scores = np.mean(ndcg_scores)
    return mean_scores
