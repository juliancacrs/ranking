import pyltr
import numpy as np
import h5py
import pandas as pd

def get_dataset_letor(k):
    dataset = list()
    with open('../dataset/letor/Fold%d/train.txt' % (k)) as trainfile, \
            open('../dataset/letor/Fold%d/vali.txt'% (k)) as valifile, \
            open('../dataset/letor/Fold%d/test.txt' % (k)) as evalfile:
        TX, Ty, Tqids, _ = pyltr.data.letor.read_dataset(trainfile)
        VX, Vy, Vqids, _ = pyltr.data.letor.read_dataset(valifile)
        EX, Ey, Eqids, _ = pyltr.data.letor.read_dataset(evalfile)
    dataset.extend([('TX',TX), ('Ty',Ty), ('Tqids', Tqids)])
    dataset.extend([('EX',EX), ('Ey',Ey), ('Eqids', Eqids)])
    dataset.extend([('VX',VX), ('Vy',Vy), ('Vqids', Vqids)])
    return dict(dataset)

def get_dataset_yahoo(k):
    dataset = list()
    data_set_path = '../dataset/ltrc_yahoo/set%d.h5'% (k)
    dsh5 = h5py.File(data_set_path)

    X_train = dsh5['/train/X']
    TX = np.zeros(X_train.shape, dtype=np.float64)
    X_train.read_direct(TX)

    y_train = dsh5['/train/y']
    Ty = np.zeros(y_train.shape, dtype=np.float64)
    y_train.read_direct(Ty)

    qids_train = dsh5['/train/X']
    Tqids = np.zeros(qids_train.shape, dtype=np.float64)
    qids_train.read_direct(Tqids)

    X_test = dsh5['/valid/X']
    EX = np.zeros(X_test.shape, dtype=np.float64)
    X_test.read_direct(EX)

    y_test = dsh5['/valid/y']
    Ey = np.zeros(y_test.shape, dtype=np.float64)
    y_test.read_direct(Ey)
    qids_test = dsh5['/valid/q']
    Eqids = np.zeros(qids_test.shape, dtype=np.float64)
    qids_test.read_direct(Eqids)
    dataset.extend([('TX',TX), ('Ty',Ty), ('Tqids', Tqids)])
    dataset.extend([('EX',EX), ('Ey',Ey), ('Eqids', Eqids)])
    dsh5.close()

    return dict(dataset)

def get_dataset_mslr10k(k):
    dataset = list()
    train = pd.read_csv('../dataset/MSLR10K/Fold%d/train_cleaned.csv'%(k))
    test = pd.read_csv('../dataset/MSLR10K/Fold%d/test_cleaned.csv'%(k))
    valid = pd.read_csv('../dataset/MSLR10K/Fold%d/vali_cleaned.csv' % (k))
    # get training parameters
    Ty = np.array(train.rel)
    Tqids = np.array(train.qid)
    train = train.drop('qid', 1)
    train = train.drop('rel', 1)
    TX = train.values

    # getting evaluation parameters
    Ey = np.array(test.rel)
    Eqids = np.array(test.qid)
    test = test.drop('qid', 1)
    test = test.drop('rel', 1)
    EX = test.values

    # getting validation parameters
    Vy = np.array(valid.rel)
    Vqids = np.array(valid.qid)
    valid = valid.drop('qid', 1)
    valid = valid.drop('rel', 1)
    VX = valid.values


    dataset.extend([('TX',TX), ('Ty',Ty), ('Tqids', Tqids)])
    dataset.extend([('EX', EX), ('Ey', Ey), ('Eqids', Eqids)])
    dataset.extend([('VX', VX), ('Vy', Vy), ('Vqids', Vqids)])

    return dict(dataset)
