import os
import h5py
from sklearn.datasets import load_svmlight_file

ltrc_dirname = '../../dataset/ltrc_yahoo/'


def transform_to_h5():
    """
    One-time transformation of the data to h5 format that is faster to load.
    """
    # this took about 10 minutes for set1
    for setname in ['set1', 'set2']:
        filename = os.path.join(ltrc_dirname, '{}.h5'.format(setname))
        f = h5py.File(filename, 'w')

        for name in ['train', 'valid', 'test']:
            g = f.create_group(name)
            filename = os.path.join(ltrc_dirname, '{}/{}.txt'.format(setname, name))
            X, y, q = load_svmlight_file(filename, query_id=True)
            g.create_dataset('X', data=X.todense(), compression='gzip')
            g.create_dataset('y', data=y, compression='gzip')
            g.create_dataset('q', data=q, compression='gzip')
        f.close()

    # Now you can do this
    #     #     f['/valid/X'].shape
    #     #     Out[24]: (71083, 699)


def main():
    print('__Processing YAHOO dataset__')
    transform_to_h5()
if __name__ == "__main__":
   main()