import numpy as np
from itertools import combinations

def pairwise_transform(x, y):
    
    '''
    Performs the pairwise transformation of the input data as described in 
    Herbrich, R., Graepel, T., & Obermayer, K. (1999). Support vector learning for ordinal regression.
    (x_i, y_i) = (x_i - x_j, sign(y_i - y_j))
    
    WARNING: may be computationally expensive due to slow itertools.combinations
    and growing size of unique combinations
    
    Arguments:
        x: input data as list, pandas dataframe or numpy array of shape (num_samples, num_features)
        y: labels as list, pandas dataframe or numpy array of shape (num_samples)
    Return:
        xpair: input data after pairwise transform as numpy array of shape (num_pairs, num_features)
        ypair: labels after pairwise transform (values either -1 or 1]) as numpy array of shape (num_pairs)
    '''
    x = np.asarray(x)
    y = np.asarray(y)
    
    #comb_iter = combinations(range(x.shape[0]), 2)
    #comb_vals = [row for row in comb_iter]
     
    xpair, ypair = list(), list()
    balance = False
    
    for i,j in combinations(range(x.shape[0]), 2):
        if y[i] == y[j]:
            continue
        else:
            xpair.append(x[i] - x[j])
            ypair.append(np.sign(y[i] - y[j]))
        if balance == True:
            balance = False
            continue
        else:
            balance = True
            xpair[-1] = np.negative(xpair[-1])
            ypair[-1] = np.negative(ypair[-1])
    
    return np.asarray(xpair), np.asarray(ypair)

def pairwise_transform2(X_train, y_train):
    # form all pairwise combinations
    comb = combinations(range(X_train.shape[0]), 2)
    k = 0
    Xp, yp, diff = [], [], []
    for (i, j) in comb:
        if y_train[i] == y_train[j]:
            continue
        Xp.append(X_train[i] - X_train[j])
        diff.append(y_train[i] - y_train[j])
        yp.append(np.sign(diff[-1]))
        # output balanced classes
        if yp[-1] != (-1) ** k:
            yp[-1] *= -1
            Xp[-1] *= -1
            diff[-1] *= -1
        k += 1
    return map(np.asanyarray, (Xp, yp, diff))

def pairwise_transform3(x_train, y_train):
    '''
     Performs the pairwise transformation for pairwise ranking
     WARNING: Computationally expensive due to lack of vectorization
     '''
    comb_vals = []

    comb_iter = combinations(range(x_train.shape[0]), 2)
    for row in comb_iter:
        comb_vals.append(row)

    xpair = []
    ypair = []
    d = []
    o = 0

    for i, j in comb_vals:
        if y_train[i] != y_train[j]:
            xpair.append(float(x_train[i]) - float(x_train[j]))
            if (float(y_train[i]) - float(y_train[j])) < 0:
                d.append(-1)
            else:
                d.append(1)
            if d[-1] < 0:
                ypair.append(-1)
            else:
                ypair.append(1)
            if ypair[-1] == (-1) ** o:
                o += 1
                continue
            else:
                o += 1
                d[-1] = np.negative(d[-1])
                xpair[-1] = np.negative(xpair[-1])
                ypair[-1] = np.negative(ypair[-1])

    return np.asarray(xpair), np.asarray(ypair)