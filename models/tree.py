#!/bin/python
# -*- coding: utf-8 -*-

import numpy as np

class Tree(object):
    def __init__(self, n_features=None, max_depth=float('inf')):
        self.feature = None
        self.n_features = n_features
        self.label = None
        self.n_samples = None
        self.gain = None
        self.left = None
        self.right = None
        self.threshold = None
        self.depth = 0
        self.max_depth = max_depth

    def build(self, X, target, criterion=None):
        if criterion is None:
            criterion = 'mse'
        features = list()
        if self.n_features is None:
            self.n_features = len(X[0])
            features = list(range(0,self.n_features))
        else:
            attributes = range(0, len(X[0]))
            features = np.random.choice(attributes, size=self.n_features, replace=False)

        self.n_samples = X.shape[0]
        if len(np.unique(target)) == 1:
            self.label = target[0]
            return

        best_gain = 0.0
        best_feature = None
        best_threshold = None

        self.label = np.mean(target)

        impurity_node = self._mse(target)
        for col in features:
            feature_level = np.unique(X[:,col])
            thresholds = (feature_level[:-1] + feature_level[1:]) / 2.0
            for threshold in thresholds:
                target_left = target[X[:,col] <= threshold]
                impurity_left = self._mse(target_left)
                n_left = float(target_left.shape[0]) / self.n_samples

                target_right = target[X[:,col] > threshold]
                impurity_right = self._mse(target_right)
                n_right = float(target_right.shape[0]) / self.n_samples

                ig = impurity_node - (n_left * impurity_left + n_right * impurity_right)

                if ig > best_gain:
                    best_gain = ig
                    best_feature = col
                    best_threshold = threshold
        self.feature = best_feature
        self.gain = best_gain
        self.threshold = best_threshold
        if self.feature is not None:
            self._divide_tree(X, target, criterion)

    def _divide_tree(self, features, target, criterion):
        if self.depth >= self.max_depth:
            self.left = None
            self.right = None
            self.feature = None
            return
        features_l = features[features[:, self.feature] <= self.threshold]
        target_l = target[features[:, self.feature] <= self.threshold]
        self.left = Tree(self.n_features, self.max_depth)
        self.left.depth = self.depth + 1
        self.left.build(features_l, target_l, criterion)

        features_r = features[features[:, self.feature] > self.threshold]
        target_r = target[features[:, self.feature] > self.threshold]
        self.right = Tree(self.n_features, self.max_depth)
        self.right.depth = self.depth + 1
        self.right.build(features_r, target_r, criterion)

    def _mse(self, target):
        y_hat = np.mean(target)
        return np.mean((target - y_hat) ** 2.0)


    def predict(self, d):
        if self.feature != None: # Node
            if d[self.feature] <= self.threshold:
                return self.left.predict(d)
            else:
                return self.right.predict(d)
        else: # Leaf
            return self.label

    def show_tree(self, depth, cond):
        base = '    ' * depth + cond
        if self.feature != None: # Node
            print(base + 'if X[' + str(self.feature) + '] <= ' + str(self.threshold))
            self.left.show_tree(depth+1, 'then ')
            self.right.show_tree(depth+1, 'else ')
        else: # Leaf
            print(base + '{value: ' + str(self.label) + ', samples: ' + str(self.n_samples) + '}')
