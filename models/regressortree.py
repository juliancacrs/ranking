#!/bin/python
# -*- coding: utf-8 -*-

import numpy as np
from .tree import Tree
from . import AdditiveModel

class RegressionTree(AdditiveModel):
    def __init__(self, criterion='mse', n_features=None, max_depth=float('inf'), min_criterion=0.05):
        self.root = None
        self.criterion = criterion
        self.max_depth = max_depth
        self.min_criterion = min_criterion
        self.n_features = n_features

    def fit(self, features, target):
        self.root = Tree(n_features=self.n_features, max_depth=self.max_depth)
        self.root.build(features, target, self.criterion)

    def predict(self, features):
        return np.array([self.root.predict(f) for f in features])

    def show_tree(self):
        self.root.show_tree(0, ' ')

class ClassificationTree(AdditiveModel):

    def fit(self, features, target):
        raise NotImplementedError()

    def predict(self, X):
        """Predict score for X.

        Parameters
        ----------
        X : array_like of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        y : array of shape [n_samples]
            The predicted scores.

        """
        raise NotImplementedError()