# -*- coding: utf-8 -*-

import numpy as np
from .regressortree import RegressionTree as DecisionTree
from . import AdditiveModel
import multiprocessing
class GradientBoosting(AdditiveModel):


    def __init__(self, n_estimators=10, learnRate=0.05, max_depth=10, n_features=None, sampling_rate=1.0, initialized=False, n_jobs = -1):
        # GBM Parameters
        if n_jobs == -1:
            self.n_jobs = multiprocessing.cpu_count()
        else:
            self.n_jobs = n_jobs

        self.n_estimators = n_estimators
        self.estimator_ = []
        self._b = []
        self._learnRate = learnRate
        self.sampling_rate = sampling_rate
        self.initialized = initialized

        # Tree Parameters
        self.max_depth = max_depth
        self.n_features = n_features

        self._current_predicted = None

    # In the paper, Friedman introduces and empirically investigates stochastic gradient boosting
    # (row-based sub-sampling). He finds that almost all subsampling percentages are better than
    #  so-called deterministic boosting and perhaps 30%-to-50% is a good value to choose on some problems and 50%-to-80% on others

    def subsample(self, X,y):
        target_index = np.random.choice(
            X.shape[0],
            int(X.shape[0] * self.sampling_rate),
            replace=False
        )
        X_sub_sampling = X[target_index]
        y_sub_samplig = y[target_index]

        return X_sub_sampling, y_sub_samplig

    def fit(self, X, y):
        Xs, ys = self.subsample(X, y)
        self._initial_approximation(X, y)


        for i in range(self.n_estimators):
            anti_grad = self.calculate_antigradient(Xs, ys)
            estimator = DecisionTree(max_depth=self.max_depth,  n_features=self.n_features)
            estimator.fit(Xs, anti_grad)
            self.estimator_.append(estimator)


    def calculate_antigradient(self, X, y):

        if self._current_predicted is not None:
            self._current_predicted += self._learnRate * self.estimator_[len(self.estimator_) - 1].predict(X)

        else:
            self._current_predicted = self.predict(X)

        q = y - self._current_predicted #Update the residual of each sample xi.

        return q


    def predict(self, X, pred_rf=None):
        y_predicted = self._get_h_0(X)

        for estimator in self.estimator_:
            y_predicted += self._learnRate*estimator.predict(X)
        if pred_rf is not None and self.initialized:
            y_predicted = y_predicted + pred_rf
        return y_predicted


    def predict_n(self, X, n):

        y_predicted = self._get_h_0(X)

        for i in range(n):
            y_predicted += self._learnRate*self.estimator_[i].predict(X)

        return y_predicted

    def _initial_approximation(self, X, y):
        self._y_mean = np.mean(y)


    def _get_h_0(self, X):
        return np.asarray([self._y_mean]*len(X))
