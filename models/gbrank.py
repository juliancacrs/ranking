from itertools import combinations
import numpy as np
#from .regressortree import RegressionTree
from sklearn.tree import DecisionTreeRegressor as RegressionTree


class GBrank(object):
    def __init__(self, n_trees=10, max_depth=5, n_features=5, sampling_rate=0.4, shrinkage=0.1, tau=0.5):

        self.n_trees = n_trees
        self.sampling_rate = sampling_rate
        self.shrinkage = shrinkage
        self.tau = tau
        self.trees = []

        # Tree Parameters
        self.max_depth = max_depth
        self.n_features = n_features

    # In the paper, Friedman introduces and empirically investigates stochastic gradient boosting
    # (row-based sub-sampling). He finds that almost all subsampling percentages are better than
    #  so-called deterministic boosting and perhaps 30%-to-50% is a good value to choose on some problems and 50%-to-80% on others

    def subsample(self, X):
        index = np.random.choice(
            X.shape[0],
            int(X.shape[0] * self.sampling_rate),
            replace=False
        )

        return index


    def fit(self, x, ys, qid_lst):

        if self.n_features is None:
            self.n_features = len(x[0])
            features = list(range(0,self.n_features))
        else:
            attributes = range(0, len(x[0]))
            features = np.random.choice(attributes, size=self.n_features, replace=False)
        X = x[:, features]

        for n_tree in range(self.n_trees):

            if n_tree == 0:
                rt = RegressionTree(max_depth=self.max_depth, random_state=0)
                rt.fit(X, ys)
                self.trees.append(rt)
                continue
            index = self.subsample(X)
            X_sample = X[index]
            y_sample = ys[index]
            qid_sample = qid_lst[index]

            ys_predict = self._predict(X_sample, n_tree)

            qid_target_distinct = np.unique(qid_sample)

            X_train_for_n_tree = []
            ys_train_for_n_tree = []
            for qid in qid_target_distinct:
                X_target_in_qid = X_sample[qid_sample == qid]
                ys_target_in_qid = y_sample[qid_sample == qid]
                ys_predict_in_qid = ys_predict[qid_sample == qid]

                for left, right in combinations(enumerate(zip(ys_target_in_qid, ys_predict_in_qid)), 2):
                    ind_1, (ys_target_1, ys_predict_1) = left
                    ind_2, (ys_target_2, ys_predict_2) = right

                    # (ys_target_1 > ys_target_2)
                    if ys_target_1 < ys_target_2:
                        ys_target_1, ys_target_2 = ys_target_2, ys_target_1
                        ys_predict_1, ys_predict_2 = ys_predict_2, ys_predict_1
                        ind_1, ind_2 = ind_2, ind_1

                    if ys_predict_1 < ys_predict_2 + self.tau:
                        X_train_for_n_tree.append(X_target_in_qid[ind_1])
                        ys_train_for_n_tree.append(ys_target_in_qid[ind_1] + self.tau)
                        X_train_for_n_tree.append(X_target_in_qid[ind_2])
                        ys_train_for_n_tree.append(ys_target_in_qid[ind_2] - self.tau)
            X_train_for_n_tree = np.array(X_train_for_n_tree)
            ys_train_for_n_tree = np.array(ys_train_for_n_tree)

            rt = RegressionTree(max_depth=self.max_depth, random_state=0)
            rt.fit(X_train_for_n_tree, ys_train_for_n_tree)
            self.trees.append(rt)

    def _predict(self, X, n_predict_trees):
        predict_lst_by_trees = [self.trees[n_tree].predict(X) for n_tree in range(n_predict_trees)]
        predict_result = predict_lst_by_trees[0]
        for n_tree in range(1, n_predict_trees):
            predict_result = (n_tree * predict_result + self.shrinkage * predict_lst_by_trees[n_tree]) / (n_tree + 1)
        return predict_result

    def predict(self, X):
        return self._predict(X, len(self.trees))