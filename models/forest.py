from __future__ import division
from math import sqrt
from .regressortree import RegressionTree
import numpy as np
from . import AdditiveModel
import multiprocessing

class RandomForest(AdditiveModel):
    def __init__(self, n_features=None, max_depth=float('inf'), n_estimators=10, boot='sqrt', n_jobs=-1):
        # RF PARAMETERS
        if n_jobs == -1:
            self.n_jobs = multiprocessing.cpu_count()
        else:
            self.n_jobs = n_jobs
        self.estimator_ = []
        self.n_estimators = n_estimators
        self.boot = boot

        # TREE PARAMETERS
        self.max_depth = max_depth
        self.n_features = n_features

    def add_tree(self, X, y):
        tree = RegressionTree(n_features=self.n_features, max_depth=self.max_depth)
        tree.fit(X,y)
        return tree


    def fit(self, X, y):
        # The API:
        # data: numpy array, rows correspond to number of data, and columns correspond to attributes
        # n_estimators: number of trees in the forest
        # maxDepth: maximum depth of each tree can grow
        # method: the prediction is continuous for regression case
        # boot: number of samples being randomly selected
        pool = multiprocessing.Pool(self.n_jobs)
        data = np.c_[X, y]
        forest = {}
        subsets = bootstrap(data, self.n_estimators, self.boot)
        # parallelizing the process for random forest..
        results = [pool.apply_async(self.add_tree, args=(subset[:, : -1],subset[:, -1],)) for subset in subsets]
        pool.close()
        pool.join()
        for i, tree in enumerate(results):
            forest[i] = tree.get()
        #forest[idx].show_tree()
        self.estimator_ = forest

    def predict(self, instance):
        # instance: the testing feature vector
        forest = self.estimator_
        value = 0.0
        for tree in forest:
            value += forest[tree].predict(instance)
        return value/len(forest)

    def cross_validation(self,data, k, n_estimators):
        #   Implementation of K-fold cross validation
        #   Most of APIs follow "learn" function
        #   k: folds
        clfs = []
        scores = []
        data = data[:]
        np.random.shuffle(data)

        #   Cross-validation
        folds = range(0,k)
        for fold in folds:
            score = 0.0  # The variable could be square of residuals or number of correct prediction w.r.t class label according to method
            training_set = np.asarray([x for i, x in enumerate(data) if i % k != fold])
            validation_set = np.asarray([x for i, x in enumerate(data) if i % k == fold])
            clf = self
            clf.learn(training_set, n_estimators, self.boot)
            for instance in validation_set:
                result = clf.predict(instance)
                score += computeError(result,instance[-1])/validation_set.shape[0]
            clfs.append(clf)
            scores.append(score)
        return clfs,scores

def bootstrap(data, numSubset, method='sqrt'):
    # randomly pick sample
    if method=='sqrt':
        numSamples = int(sqrt(data.shape[0]))
    elif method=='full':
        numSamples = data.shape[0]

    subsets = []
    for set in range(0,numSubset):
        index = np.random.randint(low=0,high=data.shape[0], size=numSamples)
        subsets.append(data[index,:])
    return subsets

def computeError(Yhat,Y):
    return  (Yhat-Y) **2
