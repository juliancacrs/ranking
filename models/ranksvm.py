from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.svm import LinearSVC
import numpy as np
from pyltr.data.pairwise_transform import pairwise_transform
from sklearn.multiclass import OneVsRestClassifier
from collections import defaultdict
import datetime
def LinearSVM():
    return OneVsRestClassifier(LinearSVC(fit_intercept=False, penalty='l1', tol=1e-3, dual=False), n_jobs=-1)


class RankSVM(BaseEstimator, RegressorMixin):
    def __init__(self, estimator=None):
        self.estimator = estimator
        if estimator is None:
            self.estimator = LinearSVM()

    def make_qdoc_dict(self,df):
        '''
        Creates dictionary of qid: features, labels
        '''
        qdoc = defaultdict()
        qid_group = df.groupby('qid')

        for qid in list(qid_group.groups.keys()):
            docs_ix = qid_group.groups[qid]
            qdoc[qid] = np.array(df.ix[docs_ix])
        return qdoc

    def get_qdoc(self,df, qid):
        '''
        Extracts qid, features, labels from a dictionary
        '''
        qdoc = self.make_qdoc_dict(df)
        x_qid = qdoc[qid][:, 2:]  # features
        y_qid = qdoc[qid][:, 0]  # labels
        return x_qid, y_qid, qid

    def subsample(self, X,y):
        target_index = np.random.choice(
            X.shape[0],
            int(X.shape[0] * 0.2),
            replace=False
        )
        X_sub_sampling = X[target_index]
        y_sub_samplig = y[target_index]

        return X_sub_sampling, y_sub_samplig

    def fit(self, df):
        xpl = []
        ypl = []
        start_ = datetime.datetime.now()
        for q in df['qid'].unique():
            x, y, q = self.get_qdoc(df, q)
            xp, yp = pairwise_transform(x, y)
            xpl.append(xp)
            ypl.append(yp)

        x_f = []
        y_f = []
        end_ = datetime.datetime.now()
        print('End Pairwise transform take = {} \n'.format(end_-start_))
        for x in xpl:
            for y in x:
                x_f.append(y)

        for y in ypl:
            for z in y:
                y_f.append(z)

        x_f = np.array(x_f)
        y_f = np.array(y_f)

        self.estimator.fit(x_f, y_f)
        self.coefs = self.estimator.coef_[0]
        return self

    def predict(self, X):
        return np.sum(self.coefs * X, 1)
